import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Form1 extends JFrame {
    private JTextField txtName;
    private JButton btnClick;
    private JPanel panelMail;
    private JCheckBox chkPerro;
    private JLabel Out;

    public Form1() {
        btnClick.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(chkPerro.isSelected())
                {
                    Out.setText("Perro");
                }

                JOptionPane.showMessageDialog(btnClick,txtName.getText()+" Hello");
            }
        });
    }

    public static void main(String[] args) {
        Form1 form = new Form1();
        form.setContentPane(form.panelMail);
        form.setSize(300,300);
        form.setVisible(true);
        form.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
